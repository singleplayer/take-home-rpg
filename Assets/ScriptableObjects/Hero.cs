﻿using UnityEngine;
using System.Collections;

namespace ErikPodetti.TakeHomeRPG
{
	[CreateAssetMenu(fileName = "Hero", menuName = "Data/Hero")]
	public class Hero : ScriptableObject
	{
		[Tooltip("Name of the hero")]
		public string Name;
		[Tooltip("Base health amount for this hero")]
		public int Health;
		[Tooltip("Base attack power for this hero")]
		public int AttackPower;
	}
}
